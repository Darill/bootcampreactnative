import React, { useState } from 'react'
import { FlatList, StyleSheet, Text, View, Image, TouchableOpacity, StatusBar } from 'react-native'
import { Data } from './dummy';
import { Ionicons, AntDesign } from '@expo/vector-icons';

const colors = {
    primary: "#517DA2",
    second: "#4EA4EA",
    caption: "#A8AAAB",
}

export default function Telegram() {
    const [drawwer, setDrawwer] = useState(true)
    const handleClick = () => {
        setDrawwer(!drawwer)
    }
    return (
        <>
        <StatusBar
        style='light'
        translucent={false}
        backgroundColor={colors.primary}
        />
            <View style={styles.container1}>
                <View style={styles.header}>
                    <View style={styles.drawwer}>
                        {drawwer ?
                            <>
                                <TouchableOpacity onPress={handleClick}>
                                    <Ionicons name="menu-outline" size={30} color="white" />
                                </TouchableOpacity>
                                <Text style={styles.textDrawwer}>Telegram</Text>
                            </> :
                            <>
                                <TouchableOpacity onPress={handleClick} style={{ position: 'absolute', left: 250, zIndex: 3 }}>
                                    <AntDesign name="close" size={24} color="black" />
                                </TouchableOpacity>
                            </>
                        }
                    </View>
                    <TouchableOpacity>
                        <Ionicons name="search" size={28} color="white" />
                    </TouchableOpacity>
                </View>
                {
                    drawwer ?
                        null : <View style={styles.drawwerTab}>
                            <View style={styles.drawwerIn}>
                                <Ionicons name="person-circle" size={100} color="black" />
                                <Text style={styles.txtName}>Daril Insan Kamil</Text>
                                <Text style={styles.txtNo}>+628151095734658</Text>
                            </View>
                            <View style={styles.drawwerIcon}>
                                <TouchableOpacity style={styles.buttonDrawwerIcon}>
                                    <Ionicons name="person-circle" size={30} color="black" />
                                    <Text style={styles.txtDrawwerIcon}>Kontak</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.buttonDrawwerIcon}>
                                    <Ionicons name="settings-outline" size={30} color="black" />
                                    <Text style={styles.txtDrawwerIcon}>Setting</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                }
            </View>

            <View style={styles.container}>
                <View>
                    {drawwer ?
                        <>
                            <FlatList
                                data={Data}
                                renderItem={({ item }) => (
                                    <>
                                        <TouchableOpacity style={styles.message}>
                                            <Image source={item.Image} style={styles.messageAvatar} />

                                            <View style={styles.nameContent}>
                                                <Text style={styles.messageName}>{item.name}</Text>
                                                <Text style={styles.messageContent}>{item.message}</Text>
                                            </View>
                                            <View style={styles.info}>
                                                <View style={styles.checkTime}>
                                                    <Ionicons name="checkmark-done" size={24} color="grey" />
                                                    <Text>{item.time}</Text>
                                                </View>
                                                <View style={styles.totalMessage}>
                                                    <Text>{item.totalMessage}</Text>
                                                </View>
                                            </View>
                                        </TouchableOpacity>

                                    </>
                                )}
                                ItemSeparatorComponent={() => <View style={styles.itemSeparator}></View>}
                            />
                            <TouchableOpacity style={styles.addTask}>
                                <Ionicons name="ios-pencil-sharp" size={30} color="white" style={styles.iconTask} />
                            </TouchableOpacity>
                        </>
                        : null
                    }
                </View>

            </View>
        </>
    )
}

const styles = StyleSheet.create({
    container1: {
        flex: 2,
        backgroundColor: colors.second,
        maxHeight: '7%',
        justifyContent: 'space-around'
    },
    drawwer: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    textDrawwer: {
        fontSize: 20,
        color: 'white',
        fontWeight: 'bold',
        paddingHorizontal: 20
    },
    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 20,
    },
    drawwerIn: {
        backgroundColor: colors.second,
        paddingHorizontal: 15,
        paddingVertical: 15,

    },
    drawwerIcon: {
        paddingHorizontal: 15,
        paddingVertical: 15,
        height: 120,
        justifyContent: 'space-between'
    },
    buttonDrawwerIcon: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    txtDrawwerIcon: {
        fontSize: 18,
        paddingHorizontal: 10
    },
    drawwerTab: {
        backgroundColor: 'rgb(255, 255, 255)',
        zIndex: 1,
        width: '80%',
        position: 'absolute',
        left: 0,
        top: 0,

    },
    txtName: {
        color: 'black',
        fontWeight: 'bold',
        fontSize: 20
    },
    txtNo: {
        color: colors.primary
    },


    container: {
        flex: 1,

    },
    addTask: {
        borderRadius: 100,
        alignItems: 'center'
    },
    iconTask: {
        position: 'absolute',
        bottom: 20,
        right: 20,
        padding: 15,
        borderRadius: 50,
        backgroundColor: colors.second,
    },
    message: {
        paddingHorizontal: 15,
        paddingVertical: 15,
        flexDirection: 'row',
        alignItems: 'center'
    },
    messageName: {
        fontSize: 20
    },
    messageAvatar: {
        width: 50,
        height: 50,
        borderRadius: 30
    },
    messageContent: {
        color: colors.caption
    },
    totalMessage: {
        padding: 5,
        backgroundColor: 'green',
        borderRadius: 50,
        alignItems: 'center',
        width: '50%',
        justifyContent: 'flex-end'
    },
    nameContent: {
        paddingHorizontal: 10,
        flex: 1
    },
    info: {
        position: 'absolute',
        right: 20,
        // justifyContent: 'space-around'
    },
    checkTime: {
        flexDirection: 'row',
        // borderWidth: 1,
        alignItems: 'center',
        justifyContent: 'space-between',
        width: '100%'
    },
    itemSeparator: {
        borderWidth: 0.2
    }
})
