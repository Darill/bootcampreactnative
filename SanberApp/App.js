import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Tugas14 from './Tugas 14/tugas14';
import Telegram from './tugas12/Telegram';
import AboutScreen from './Tugas13/aboutScreen';
import LoginScreen from './Tugas13/loginScreen';
import Index from './Quiz3';
import StackNavigation from './Tugas 15/navigate';


export default function App() {
  return (
    <View style={styles.container}>
      {/* <StackNavigation/> */}
      <Index/>
      
      <StatusBar style="light" translucent={false}/>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
