import React from "react";
import { StyleSheet, Text, View, TextInput, TouchableOpacity } from "react-native";
import { Ionicons, AntDesign } from '@expo/vector-icons';
const Headers = () => {
    return (
        <View style={styles.container}>
            <TextInput
                placeholder="Search"
                placeholderTextColor="grey"
                style={styles.textinput}
            />
            <TouchableOpacity>
                <Ionicons name="menu-outline" size={22} color="black" />
            </TouchableOpacity>
            <TouchableOpacity>
                <Ionicons name="person" size={22} color="black" />
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 0.1,
        // borderBottomWidth: 0.2,
        elevation: 5,
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 20,
        paddingVertical: 5,
        justifyContent: 'space-around',
        backgroundColor: 'white'
    },
    titleHeader: {
        fontSize: 20,
        fontWeight: 'bold',
        color: 'white'
    },
    textinput: {
        height: 40,
        width: '80%',
        borderWidth: 0.5,
        borderRadius: 10,
        paddingHorizontal: 5,
        borderColor: 'black',
        color: 'black'
    }
})
export default Headers;