import React, { useContext, useState } from "react";
import { FlatList, Text, TextInput, TouchableOpacity, Image, ActivityIndicator, StyleSheet, View } from "react-native";
import { MovieContext } from "../context/fetch";
import { AntDesign, Ionicons } from '@expo/vector-icons';
import Headers from "./headers";



const Home = ({ navigation }) => {
    const [data, setData, loadMore, setLoadMore, loading, setLoadin] = useContext(MovieContext)
    console.log(data)
    return (
        <>
            {/* Headers */}
            <Headers />
            <View style={styles.container}>

                {/* Load */}

                {
                    loading ? <View style={styles.loading}>
                        <ActivityIndicator size="small" color="#0000ff" />
                        <Text>Tunggu Sebentar...</Text>
                    </View> : null
                }

                {/* Data */}
                <FlatList
                    style={styles.flatlist}
                    data={data}
                    numColumns={2}
                    keyExtractor={(item, index) => `${item.id}-${index}`}
                    renderItem={({ item }) => (
                        <TouchableOpacity style={styles.containerList} onPress={() => navigation.navigate("Detail", { title: item.title, image: item.image, price: item.price, desc: item.description, rate: item.rating.rate, count: item.rating.count, category:item.category })}>
                            <Image source={{ uri: item.image }} style={styles.imageList} />
                            <Text style={styles.title}>{item.title}</Text>
                            <Text style={styles.price}>${item.price}</Text>
                            <View style={styles.rate}>
                                <AntDesign name="star" size={18} style={{ paddingRight: 2 }} color="yellow" />
                                <Text >{item.rating.rate} | </Text>
                                <Text >Terjual {item.rating.count}</Text>
                            </View>
                        </TouchableOpacity>
                    )}
                />
            </View>
        </>
    )
}

const styles = StyleSheet.create({

    // Headers

    // FlatList

    container: {
        flex: 1
    },
    loading: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    flatlist: {
        paddingBottom: 100
    },
    imageList: {
        width: 150,
        height: 200,
        alignSelf:'center'
    },
    title: {
        fontSize: 17,
        paddingTop: 5
    },
    price: {
        fontWeight: 'bold',
        fontSize: 17,
        paddingVertical: 5
    },
    rate: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    containerList: {
        flex: 1,
        margin: 5,
        paddingHorizontal: 25,
        paddingVertical: 20,
        justifyContent: 'space-around',
        borderBottomWidth: 0.5,
        backgroundColor:'white',
        elevation: 5,
        borderRadius: 20
    }
})
export default Home