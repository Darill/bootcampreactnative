import React from 'react'
import { View, Text, TouchableOpacity, Image, StyleSheet, ScrollView } from 'react-native'
import { Ionicons, AntDesign } from '@expo/vector-icons';
const DetailScreen = ({ route }) => {
    const { title, image, price, desc, rate, count, category } = route.params
    return (
        <View style={styles.container}>
            <ScrollView style={{ marginBottom: 60 }}>
                <View style={styles.containImage}>
                    <Image style={styles.image} source={{ uri: image }} />
                </View>
                <View style={styles.content}>
                    <View style={{ backgroundColor: 'white', paddingHorizontal: 20 }}>
                        <Text style={styles.price}>${price}</Text>
                        <Text style={styles.title}>{title}</Text>
                        <View style={styles.rateContain}>
                            <Text >Terjual {count}</Text>
                            <View style={styles.rate}>
                                <AntDesign name="star" size={18} style={{ paddingRight: 2 }} color="yellow" />
                                <Text>{rate}</Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.Detail}>
                        <Text style={styles.price}>Detail Produk</Text>

                        <View style={styles.etalase}>
                            <Text>Jumlah</Text>
                            <Text>1 Buah</Text>
                        </View>
                        <View style={styles.etalase}>
                            <Text>Etalase</Text>
                            <Text>{category}</Text>
                        </View>
                        <Text style={{marginTop: 20, fontSize: 16}}>{desc}</Text>
                    </View>
                </View>
            </ScrollView>
            <View style={styles.buttonContain}>
                <TouchableOpacity style={styles.icon}>
                    <Ionicons name="chatbubble-ellipses-outline" size={24} color="black" />
                </TouchableOpacity>
                <TouchableOpacity style={styles.keranjangContain}>
                    <Text style={styles.keranjang}>+ Keranjang</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.beliContain} onPress={() => alert("Belum berfungsi")}>
                    <Text style={styles.beli}>Beli</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    containImage: {
        backgroundColor: 'white',
        marginTop: 5,
        width: '100%',
        paddingVertical: 40
    },
    image: {
        height: 450,
        width: 300,
        alignSelf: 'center',
    },
    price: {
        fontWeight: 'bold',
        fontSize: 24
    },
    title: {
        fontSize: 18,
        marginTop: 10
    },
    rateContain: {
        flexDirection: 'row',
        alignItems: 'center',
        marginVertical: 10
    },
    rate: {
        flexDirection: 'row',
        borderWidth: 1,
        borderColor: 'grey',
        paddingHorizontal: 8,
        paddingVertical: 3,
        borderRadius: 5,
        marginLeft: 10
    },
    buttonContain: {
        position: 'absolute',
        bottom: 0,
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        width: '100%',
        paddingVertical: 15,
        zIndex: 100,
        backgroundColor: 'white',
        elevation: 5
    },
    keranjangContain: {
        borderWidth: 1.5,
        borderColor: 'green',
        paddingVertical: 10,
        paddingHorizontal: 10,
        width: '40%',
        borderRadius: 10
    },
    beliContain: {
        width: '40%',
        backgroundColor: 'green',
        paddingVertical: 10,
        paddingHorizontal: 10,
        borderRadius: 10
    },
    icon: {
        paddingVertical: 10,
        paddingHorizontal: 10,
        borderRadius: 10,
        borderWidth: 1.5,
        borderColor: 'grey',
    },
    keranjang: {
        textAlign: 'center',
        fontSize: 18,
        color: 'green',
        fontWeight: 'bold'
    },
    beli: {
        fontWeight: 'bold',
        textAlign: 'center',
        color: 'white',
        fontSize: 18,
    },
    Detail: {
        paddingHorizontal: 20,
        paddingVertical: 10,
        backgroundColor: 'white',
        marginTop: 5,
        marginBottom: 25
    },
    etalase: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderBottomWidth: 1,
        borderColor: 'green',
        padding: 5,
        margin: 10,
    },
})
export default DetailScreen


