import React from "react";
import StackScreen from "../component/StackScreen";
import { MovieProvider } from "../context/fetch";

const MovieRender = () => {
    return (
        <MovieProvider>
            <StackScreen/>
        </MovieProvider>
    )
}

export default MovieRender