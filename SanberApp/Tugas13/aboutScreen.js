import React, { useContext, useState } from 'react'
import { FlatList, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { Ionicons } from '@expo/vector-icons';
import { LoginContext } from '../Tugas 16/context';

export default function AboutScreen({navigation}) {
    const [pressSkill, setPressSkill] = useState(true)
    const [auth, setAuth, loginStatus, setLoginStatus] = useContext(LoginContext)
    const handlePress = () => {
        setPressSkill(!pressSkill)
    }
    // console.log(auth)
    return (
        <>
            <View style={styles.container}>
                <View style={styles.profileWrapper}>
                    <Ionicons name="ios-person-circle" size={110} color="black" />
                    <View style={styles.profile}>
                        {
                            loginStatus ? <></> : <FlatList
                                data={auth}
                                keyExtractor={(item, index) => `${item.id}-${index} `}
                                renderItem={({ item }) => {
                                    return (
                                        <>
                                            <Text>{item.email}</Text>
                                        </>
                                    )
                                }}
                            />
                        }
                        <Text style={styles.profileClass}>beginner</Text>
                        <TouchableOpacity style={styles.setting}>
                            <Ionicons name="settings-outline" size={24} color="black" style={styles.settingIcon} />
                            <Text style={styles.settingProfile}>edit profile</Text>
                        </TouchableOpacity>
                    </View>

                </View>

                <View style={styles.content}>
                    {
                        pressSkill ?
                            <View style={styles.skill}>
                                <Text>HTML</Text>
                                <View style={styles.row}>
                                    <View style={styles.htmlBar}></View>
                                    <Text style={styles.rowTxt}>90%</Text>
                                </View>
                                <Text>CSS</Text>
                                <View style={styles.row}>
                                    <View style={styles.cssBar}></View>
                                    <Text style={styles.rowTxt}>90%</Text>
                                </View>
                                <Text>Javascript</Text>
                                <View style={styles.row}>
                                    <View style={styles.javascriptBar}></View>
                                    <Text style={styles.rowTxt}>75%</Text>
                                </View>
                                <Text>ReactJs</Text>
                                <View style={styles.row}>
                                    <View style={styles.reacJstBar}></View>
                                    <Text style={styles.rowTxt}>60%</Text>
                                </View>
                                <Text>React Native</Text>
                                <View style={styles.row}>
                                    <View style={styles.reactNativeBar}></View>
                                    <Text style={styles.rowTxt}>30%</Text>
                                </View>
                            </View> : null
                    }
                    {pressSkill ?
                        <Ionicons name="close" size={24} color="black" onPress={handlePress} />
                        :
                        <>
                            <Text style={styles.txtContent} onPress={handlePress}>Skill</Text>
                            <Text style={styles.txtContent} >Project</Text>
                        </>
                    }
                </View>


                <View style={styles.contactWrapper}>
                    <Ionicons name="logo-linkedin" size={34} color="black" />
                    <Ionicons name="logo-github" size={34} color="black" />
                    <Ionicons name="logo-instagram" size={34} color="black" />
                </View>
            </View>
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 20
    },
    profileWrapper: {
        alignItems: 'center',
        flexDirection: 'row',
        // alignSelf: 'center',
        marginTop: 30,
    },
    profileName: {
        fontWeight: 'bold',
        fontSize: 18
    },
    profileClass: {
        color: 'grey',
        marginVertical: 5
    },
    setting: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    settingProfile: {
        borderWidth: 1,
        padding: 5,
        paddingHorizontal: 10,
        borderRadius: 30,
        fontSize: 12
    },
    settingIcon: {
        marginRight: 5
    },

    content: {
        alignSelf: 'center',
        borderBottomWidth: 1,
        padding: 5,
        width: '100%',
        marginVertical: 10,
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    txtContent: {
        fontSize: 16,
    },

    press: {
        borderBottomWidth: 4,
        borderColor: 'red'
    },

    skill: {
        // alignSelf: 'center',
        paddingVertical: 20,
        paddingHorizontal: 20,
        width: '85%'
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    rowTxt: {
        paddingLeft: 5
    },
    htmlBar: {
        borderBottomWidth: 5,
        borderColor: '#C4C4C4',
        width: '90%'
    },
    cssBar: {
        borderBottomWidth: 5,
        borderColor: '#C4C4C4',
        width: '90%'
    },
    javascriptBar: {
        borderBottomWidth: 5,
        borderColor: '#C4C4C4',
        width: '75%'
    },
    reacJstBar: {
        borderBottomWidth: 5,
        borderColor: '#C4C4C4',
        width: '60%'
    },
    reactNativeBar: {
        borderBottomWidth: 5,
        borderColor: '#C4C4C4',
        width: '30%'
    },

    contactWrapper: {
        flexDirection: 'row',
        alignSelf: 'center',
        marginVertical: 30,
        width: '50%',
        justifyContent: 'space-around'
    },
    projectWrapper: {
        width: '85%',
        // borderWidth: 1
    }
})
