import React, { createContext, useState } from "react";
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import AboutScreen from "../Tugas13/aboutScreen";

export const LoginContext = createContext()

const Stack = createNativeStackNavigator()

export const LoginProvider = ({ children }) => {
    const [loginStatus, setLoginStatus] = useState(false)
    const [isError, setIsError] = useState(false)
    const [auth, setAuth] = useState([
        {
            email: null,
            password: null
        }
    ])

    return (
        <LoginContext.Provider value={
            [auth, setAuth, loginStatus, setLoginStatus, isError, setIsError]
        }>
            {children}
        </LoginContext.Provider>
    )
}