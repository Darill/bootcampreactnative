console.log("SOAL 1");
console.log();

function shout () {
	return "Halo Sanbers!"
}

console.log(shout())

// Cara ke-2
// function shout1() {
//     console.log("Hallo Sanbers!");
// }
// shout1()

console.log();
console.log("SOAL 2");
console.log();

var num1 = 12
var num2 = 4
function kalikan(num1, num2) {
    return num1 * num2
}
var hasilKali = kalikan(num1, num2);
console.log(hasilKali);

// Cara ke-2
// function kalikan2 (nums1, nums2){
//     return nums1 * nums2
// }
// console.log(kalikan2(12, 4));

console.log();
console.log("SOAL 3");
console.log();

var name = "Agus";
var age = 30;
var address = "Jln. Malioboro, Yogyakarta";
var hobby = "Gaming";
// "Nama saya Agus, umur saya 30 tahun, alamat saya di Jln. Malioboro, Yogyakarta, dan saya punya hobby yaitu Gaming!" 

function introduce(name, age, address, hobby) {
    return (
        "Nama saya " + name + ", umur saya " + age + " tahun, alamat saya di " + address + ", dan saya punya hobby yaitu " + hobby + "!" 
    )
}

var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan);