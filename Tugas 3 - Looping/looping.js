console.log("SOAL 1");
console.log();
// While Looping
console.log("LOOPING PERTAMA");

var a = 2;
while (a <= 20 ) {
    console.log(a + " - I love coding");
    a += 2
};

console.log("LOOPING KEDUA");

var b = 20; 
while (b >= 2) {
    console.log(b + " - I will become a mobile developer");
    b -= 2
}

console.log();
console.log('SOAL 2');
console.log();

// A. Jika angka ganjil maka tampilkan Santai
// B. Jika angka genap maka tampilkan Berkualitas
// C. Jika angka yang sedang ditampilkan adalah kelipatan 3 dan angka ganjil maka tampilkan I Love Coding.
//For loop mengunakan conditional

for (let a = 1; a <= 20 ; a++) {
    if (a % 2 === 1 && a % 3 === 0) {
        console.log(a + " - I love coding");
    } else if (a % 2 === 1) {
        console.log(a + " - Santai");
    } else {
        console.log(a + " - Berkualitas");
    }

}

console.log();
console.log("SOAL 3");
console.log();

//For loop Table

for (let sharp = 1; sharp <= 4; sharp++) {
    console.log("#######");
}

console.log();
console.log("SOAL 4");
console.log();
//For Loop stair


var a = " ";
for (let stair = 1; stair <= 7; stair++) {
    console.log(a+= "#");
}

console.log();
console.log("SOAL 5");
console.log();

//For Loop Chess

for (let chess = 1; chess <= 8; chess++) {
    if (chess % 2 === 1) {
        console.log(" # # # #");
    } else {
        console.log("# # # #");
    }
    
}
