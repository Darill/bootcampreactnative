
console.log("Array to Object\n");
function arrayToObject(arr) {
    if (arr.length > 0) {
        var object = {}
        var now = new Date()
        var thisYear = now.getFullYear()
        for (let i = 0; i < arr.length; i++) {
            var number = i + 1
            object[number + ". " + arr[i][0] + ' ' + arr[i][1]] = {
                firstName: arr[i][0],
                lastName: arr[i][1],
                gender: arr[i][2],
                age: thisYear - arr[i][3] > 0 && arr[i][3] != undefined ? thisYear - arr[i][3] : 'Invalid Birth Year'
            }
        }
        console.log(object);
    } else {
        console.log("");
    }
}

var people = [["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"]]
arrayToObject(people)
var people2 = [["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023]]
arrayToObject(people2)
arrayToObject([])

console.log("\nShopping Time\n");
function shoppingTime(memberId, money) {
    if (!memberId || !money) {
        return "Mohon maaf, toko X hanya berlaku unutk member saja"
    } else {
        var items = belanjaan()
        var output = { memberId, money, listPurchased: [], changeMoney: money }
        for (const key in items) {
            if (output.changeMoney >= items[key]) {
                output.listPurchased.push(key)
                output.changeMoney -= items[key]
            }
        }
        if (output.listPurchased.length < 1) {
            return "Mohon maaf, unag tidak cukup"
        } else {
            return output
        }
    }
}

function belanjaan() {
    return {
        "Sepatu Stacattu": 1500000,
        "Baju Zoro": 500000,
        "Baju H&N": 250000,
        "Sweater Unikloh": 175000,
        "Casing HandPhone": 50000,
    }
}
console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

console.log("\nNaik Angkot\n");

function naikAngkot(arrPenumpang) {
    var rute = ['A', 'B', 'C', 'D', 'E', 'F']
    var penumpangArr = []
    if (arrPenumpang.length > 0) {
        arrPenumpang.forEach((penumpangNaik, index) => {
            penumpangArr[index] = {
                penumpang: penumpangNaik[0],
                naikDari: penumpangNaik[1],
                tujuan: penumpangNaik[2],
                bayar: 0
            }
            rute.forEach(route => {
                if (route > penumpangNaik[1] && route <= penumpangNaik[2]) {
                    penumpangArr[index].bayar += 2000
                }
            })
        })
    }
    return penumpangArr
}
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
console.log(naikAngkot([]))

