console.log("\nSOAL 1\n");
function range(start, end) {
    var ranges = []
    if (start < end) {
        for (let index = start; index <= end; index++) {
            ranges.push(index)
        }
    } else if (start > end)
        for (let index = start; index >= end; index--) {
            ranges.push(index)
        } else {
        return -1
    }
    return ranges
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11, 18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

console.log("\nSOAL 2\n");

function rangeWithStep(start, end, step) {
    var ranges = []
    if (start < end) {
        for (let index = start; index <= end; index += step) {
            ranges.push(index)
        }
    } else if (start > end) {
        for (let index = start; index >= end; index -= step) {
            ranges.push(index)
        }
    }
    return ranges
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

console.log("\nSOAL 3\n");
function sum(start, end, step = 1) {
    var ranges = []
    var reducer = (a, b) => a +b;
    if (start === undefined && end === undefined) {
        return 0
    } else if (start < end) {
        for (let index = start; index <= end; index += step) {
            ranges.push(index)
        }
        // return ranges.reduce((a, b) => a + b, 0);
    } else if (start > end) {
        for (let index = start; index >= end; index -= step) {
            ranges.push(index)
        }
        // return ranges.reduce((a, b) => a + b, 0);
    } else {
        return 1
    }
    return ranges.reduce(reducer)
}

console.log(sum(1, 10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15, 10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

console.log("\nSOAL 4\n")
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]
// menggunakan forEach
function dataHandling(input) {
    var foreachs = input.forEach(element => {
        console.log("ID: " + element[0])
        console.log("Nama Lengkap: " + element[1])
        console.log("TTL: " + element[2] + " " + element[3])
        console.log("Hobi: " + element[4] + "\n")
    });
    return foreachs
}
dataHandling(input)

// menggunakan for of
// function dataHandlings(input) {
//     for (var newArray of input) {
//         console.log("ID: " + newArray[0])
//         console.log("Nama Lengkap: " + newArray[1])
//         console.log("TTL: " + newArray[2] + " " + newArray[3])
//         console.log("Hobi: " + newArray[4])
//         console.log()
//     }
// }
// dataHandlings(input)



console.log("\nSOAL 5\n")

function balikKata(str) {
    var str1 = str
    var newString = ""

    for (let index = str1.length - 1; index >= 0; index--) {
        newString = newString + str1[index]
    }
    return newString
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

console.log("\nSOAL 6\n");

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
function dataHandling2(input) {
    //task1
    input.splice(1, 2, "Roman Alamsyah Elsharawy ", "Provinsi Bandar Lampung")
    input.splice(4, 1, "Pria", "SMA Internasional Metro")
    console.log(input);
    //task2
    var date = input[3].split("/")
    console.log(format(date[1]))
    var dmy = date.sort(function (value1, value2) {
        return value2 - value1
    })
    console.log(dmy)
    date.shift()
    date.push("1989")
    console.log(date.join("-"))
    //task3
    var name = input[1].slice(0, 14)
    console.log(name);
}
dataHandling2(input)

function format(bulan) {
    switch (bulan) {
        case '01':
            return "Januari"
            break;
        case '02':
            return "Februaru"
            break;
        case '03':
            return "Maret"
            break;
        case '04':
            return "April"
            break;
        case '05':
            return "Mei"
            break;
        case '06':
            return "Juni"
            break;
        case '07':
            return "Juli"
            break;
        case '08':
            return "Agustus"
            break;
        case '09':
            return "September"
            break;
        case '10':
            return "Oktober"
            break;
        case '11':
            return "November"
            break;
        case '12':
            return "Desember"
            break;
        default:
            break;
    }
}


/**
 * keluaran yang diharapkan (pada console)
 *
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
 */