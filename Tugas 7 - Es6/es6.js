console.log('Soal 1\n');
//mengubah function menjadi arrow function

const golden = () => {
    console.log("this is golden!!");
}
golden()

console.log('\nSoal 2\n');

//menyederhanakan menjadi object literal

const newFunction = function literal(firstName, lastName) {
    return {
        firstName,
        lastName,
        fullName: function () {
            console.log(`${firstName} ${lastName}`)
            return
        }
    }
}
const person = newFunction("William", "Imoh");
console.log(person);
person.fullName();

console.log('\nSoal 3\n');
//Destructuring

const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}
const { firstName, lastName, destination, occupation } = newObject
console.log(firstName, lastName, destination, occupation)

console.log('\nSoal 4\n');
//Array Sepreading
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]
console.log(combined);

console.log('\nSoal 5\n');
//template literal
const planet = "earth"
const view = "glass"
var before = `Lorem ${view} dolor sit amet,  
consectetur adipiscing elit, ${planet} do eiusmod tempor  
incididunt ut labore et dolore magna aliqua. Ut enim 
ad minim veniam`
console.log(before)