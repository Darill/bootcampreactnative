const readBooksPromise = require('./promise');

const books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 }
]

readBooksPromise(10000, books[0])
    .then(waktu => {
        return readBooksPromise(waktu, books[1])
    })
    .then(waktu1 => {
        return readBooksPromise(waktu1, books[2])
    })
    .catch(err => {
        console.log(err)
    })