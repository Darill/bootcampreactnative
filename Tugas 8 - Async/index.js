const readBooks = require('./callback')

const books = [
    {
        name: 'LOTR',
        timeSpent: 3000
    },
    {
        name: 'Fidas',
        timeSpent: 2000
    },
    {
        name: 'Kalkulus',
        timeSpent: 4000
    },
]

readBooks(10000, books[0], (remainingTime) => {
    readBooks(remainingTime, books[1], (remainingTime) => {
        readBooks(remainingTime, books[2], (remainingTime)=> {
            console.log("waktu saya habis");
        })
    })
})